
# Power Shell shortcut and profile for Thinkpad Laptop

this is my customized Power Shell profile and a shortcut

## PowerShell Invoke-WebRequest could not create SSL TLS secure channel

if the iwr command can't create a TLS secure channel it's necessary to change the TLS version

```powershell
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
```

## remap the capslock key with escape

the script `remap-capslocktoescape.ps1` remaps the caps lock key to escape

### usage

- run the script as administrator
- reboot the system

## Power Shell profile 

the file `Microsoft.PowerShell_profile.ps1` includes a custom prompt, aliases and some functions

### installation

copy the content of this file to the current profile file

## Power Shell shortcut

the file `Windows PowerShell.lnk` is a customized shotcut to Power Shell with the **GruvBox dark** color scheme

### installation

download the shortcut with Power Shell

```powershell
iwr -Uri https://gitlab.com/24F431/PowerShell-ThinkPad/-/raw/master/Windows%20PowerShell.lnk -OutFile "Windows PowerShell.lnk"
```

copy the file to any directory

## create-sqlfirewallrule.ps1 script

this script open the necessary ports for SQL Server in the Windows Firewall

## check-TLS1.2.ps1 script

this script checks for TLS 1.2 in the registry

## enable-TLS1.2.ps1 script

this script enables TLS 1.2 in the registry

### usage

- run the script as administrator
- reboot the system

## backup-vms.ps1 script

this script exports Microsoft Hyper-V virtual machines to external harddrive

## copy-DBbackup.ps1 script

this script copies backup files to destination Server

### usage

- download the script

    ```powershell
    Invoke-WebRequest -Uri https://gitlab.com/24F431/PowerShell-ThinkPad/-/raw/master/copy-DBbackup.ps1 -OutFile copy-DBbackup.ps1
    ```
- modify source, destination and username

## dcom scripts

this scripts are for testing purpose of the `Microsoft Update - DCOM Hardening`

### check-dcom.ps1 script

this script prints the registry keys under the path `HKLM:\SOFTWARE\Microsoft\Ole\AppCompat`  
if the value of the key `RequireIntegrityActivationAuthenticationLevel` is 1 it's enabled

### enable-dcom.ps1 script

this script enables the registry key for the DCOM Hardening

### disable-dcom.ps1 script

this script disables the registry key for the DCOM Hardening
