################################################################################
# script creates the registry keys for dcom                                    #
# system reboot is required                                                    #
################################################################################
$path = "HKLM:\SOFTWARE\Microsoft\Ole\AppCompat"
$name = "RequireIntegrityActivationAuthenticationLevel"
Write-Host -ForegroundColor DarkGreen "enabeling dcom"
If (!(Test-Path $path)) {
    New-Item -Path $path -Force
}
if (Get-ItemProperty -Path $path -Name $name -ErrorAction Ignore ) {
    Set-ItemProperty -Name $name -Path $path -Value 1
} else {
    New-ItemProperty -Path $path -Name $name -PropertyType DWord -Value 1
}