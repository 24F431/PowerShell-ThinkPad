﻿<#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$ script for backup virtual machines                                           $
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$#>
$backupPath = "F:\backup virtual machines"
$backupPath
$pathDate = Get-Date -Format "yyyy-MM-dd"
$pathDate
$fullPath = $backupPath + "\" + $pathDate
$fullPath
if (Test-Path $backupPath) {
    if (-not (Test-Path $fullPath)) {
        Write-Host -ForegroundColor yellow -Object "create path $fullPath"
        mkdir $fullPath
    }
} else {
    Write-Error "[$backupPath] not found"
    return $false
}
$myVMnameArr =@('cra-mhs-rep-dev'
'cra-mhs-app-ts-da'
'cra-mhs-hist'
'cra-mhs-rmp')
if (Test-Path $fullPath) {
   $summaryVMExportArr = @()
    foreach ($loopVMname in $myVMnameArr) {
        # exporting vm's
        Write-Host -ForegroundColor yellow -Object "exporting $loopVMname"
        $startTime = Get-Date
        echo "StartTime $startTime"
        #Start-Sleep -Seconds 5
        Export-VM -Name $loopVMname -path $fullPath
        $exportStatus = $?
        $endTime = Get-Date
        echo "EndTime $endTime"
        $loopObject = New-Object PSCustomObject
        $loopObject | Add-Member -type NoteProperty -Name VMName -Value $loopVMname
        $loopObject | Add-Member -type NoteProperty -Name StartTime -Value $startTime
        $loopObject | Add-Member -type NoteProperty -Name EndTime -Value $endTime
        $loopObject | Add-Member -type NoteProperty -Name Duration -Value (New-TimeSpan -Start $startTime -End $endTime)
        $loopObject | Add-Member -type NoteProperty -Name ExportOK -Value $exportStatus
        $summaryVMExportArr += $loopObject
    }
    echo "" "summary" "-------"
    $summaryVMExportArr | Format-Table
}