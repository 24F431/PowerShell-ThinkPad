<###############################################################################
# script checks the registry keys for TLS 1.2                                  #
###############################################################################>
$paths = @(
"HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client"
"HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server"
)
Write-Host -ForegroundColor DarkGreen "checking TLS 1.2"
Get-ItemProperty -Path $paths | Format-Table -Wrap