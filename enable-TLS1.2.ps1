<###############################################################################
# script creates the registry keys for TLS 1.2                                 #
# system reboot is required                                                    #
###############################################################################>
$paths = @(
"HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client"
"HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server"
)
Write-Host "enabeling TLS 1.2"
foreach ($loopPath in $paths) {
    If (!(Test-Path $loopPath)) {
        New-Item -Path $loopPath -Force
    }
    Set-ItemProperty -Path $loopPath -Name "DisabledByDefault" -Type DWord -Value 0
    Set-ItemProperty -Path $loopPath -Name "Enabled" -Type DWord -Value 1
}
