﻿################################################################################
# script remaps the caps lock key with escape                                  #
# run with admin privileges                                                    #
# system reboot is required                                                    #
################################################################################
$path = "HKLM:\SYSTEM\CurrentControlSet\Control\Keyboard Layout"
$name = "Scancode Map"
$value = [byte[]](0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x02,0x00,0x00,0x00,
    0x01,0x00,0x3a,0x00,0x00,0x00,0x00,0x00
)
Write-Host -ForegroundColor DarkGreen "remapping caps lock key with escape"
if (Get-ItemProperty -Path $path -Name $name -ErrorAction Ignore ) {
    Set-ItemProperty -Name $name -Path $path -Value $value
} else {
    New-ItemProperty -Path $path -Name $name -PropertyType Binary -Value $value
}