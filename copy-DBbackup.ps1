################################################################################
# script copies database backup files to destination Server                    #
#                                                                              #
################################################################################

# open network share
Write-Host -ForegroundColor DarkYellow "1/3 open network share"
net use \\computername /user:domain\username
if ($?) {
    # copy all files that are newer to network share
    Write-Host -ForegroundColor DarkYellow "2/3 copy files to network share"
    robocopy "$env:PUBLIC\SQL DB" "\\computername" "*.bak" /XO /XN
    # close network share
    Write-Host -ForegroundColor DarkYellow "3/3 close network share"
    net use \\computername /D
}

