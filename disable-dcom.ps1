################################################################################
# script disables the registry keys for dcom                                   #
# system reboot is required                                                    #
################################################################################
$path = "HKLM:\SOFTWARE\Microsoft\Ole\AppCompat"
$name = "RequireIntegrityActivationAuthenticationLevel"
Write-Host -ForegroundColor DarkGreen "disabeling dcom"
if (Get-ItemProperty -Path $path -Name $name -ErrorAction Ignore ) {
    Set-ItemProperty -Name $name -Path $path -Value 0
}