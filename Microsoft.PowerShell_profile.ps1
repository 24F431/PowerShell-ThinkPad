﻿# powershell profile
#                                   _          _ _ 
#  _ __   _____      _____ _ __ ___| |__   ___| | |
# | '_ \ / _ \ \ /\ / / _ \ '__/ __| '_ \ / _ \ | |
# | |_) | (_) \ V  V /  __/ |  \__ \ | | |  __/ | |
# | .__/ \___/ \_/\_/ \___|_|  |___/_| |_|\___|_|_|
# |_|                                              
# 
Set-Alias -Name 'n++' -Value "C:\Program Files\Notepad++\notepad++.exe"
Set-Alias -Name 'scalc' -Value "C:\Program Files\LibreOffice\program\scalc.exe"
# device manager
Set-Alias -Name 'gmg' -Value "devmgmt.msc"
Set-Alias -Name 'catc' -Value "C:\Users\ThinkPad\Apps\cygwin\bin\highlight.exe"
Set-Alias -Name 'store' -Value "C:\Users\ThinkPad\Microsoft Store.lnk"
Set-Alias -Name 'gvim' -Value "C:\Users\ThinkPad\Apps\gVimPortable\gVimPortable.exe"
Set-Alias -Name 'grep' -Value 'Select-String'
Set-Alias -Name 'system' -Value 'sysdm.cpl'
# edit this file in vim
function pro { C:\Users\ThinkPad\Apps\cygwin\bin\vim.exe /cygdrive/c/Users/ThinkPad/Documents/WindowsPowerShell/Microsoft.PowerShell_profile.ps1 }
function space { get-psdrive -psprovider 'filesystem' |
    format-table -Property 'Name',
        @{Name = 'Used (GB)'; Expression = { "{0:F2}" -f (($_.Used)/1Gb) }; Align="right"},
        @{Name = 'Free (GB)'; Expression = { "{0:F2}" -f (($_.Free)/1Gb) }; Align="right"},
        'Description','Root' -AutoSize
}
function get-mymem { get-process | Group-Object -Property ProcessName,Description |
    % {
        [PSCustomObject]@{
            ProcessName = ($_.Name -split ',')[0]
            Description = ($_.Name -split ', ')[1]
            "Mem (MB)" = [math]::Round(($_.Group|Measure-Object WorkingSet64 -Sum).Sum / 1MB, 0)
            ProcessCount = $_.Count
        }
    } | sort -Descending "Mem (MB)" | Select-Object -First 25
}
function ver { Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\" | select-object ProductName,ReleaseID }
function pi2 { ssh pi@pi2.fritz.box }
function phosts { C:\Users\ThinkPad\Apps\cygwin\bin\highlight.exe --syntax=sh C:\Windows\System32\drivers\etc\hosts }
function pp { $env:path -split ";" }
function penv { Get-ChildItem -Path Env: }
function dns { [System.Net.DNS]::GetHostByName($Null) }
function mydate { 
    #Get-Date -Format 'yyyy-MM-dd_HHmmss' | tee -Variable mydate; $mydate | Set-Clipboard
    $mydate = Get-Date -Format 'yyyy-MM-dd_HHmmss'
    Write-Host -ForegroundColor DarkGreen -NoNewline " "
    Write-Host $mydate
    $mydate | Set-Clipboard
}
function myip { Write-Host -ForegroundColor DarkCyan ' IPv4Address of connected Interface  '; Get-NetIPInterface | ? ConnectionState -EQ 'Connected' | ? InterfaceAlias -NotLike 'Loopback*' | Get-NetIPAddress | select -Property InterfaceAlias,IPAddress,AddressFamily | ? AddressFamily -EQ 'IPv4' }
function sound { Start-Process -FilePath 'ms-settings:sound' }
function update { Start-Process -FilePath 'ms-settings:windowsupdate' }
function SmartClient {
    Write-Host -ForegroundColor Yellow -NoNewline "[i]"
    Write-Host " Siemens SmartClient_V17 starten"
    & "D:\share\edistherm\remote access\SmartClient_V17.exe"
}
function get-FolderSize {
    param ( $Path )
    if ( Test-Path $Path ) {
        (ls -path $Path -Recurse -file | Measure -Property Length -sum).sum / 1Gb
    }
}
function find-file { 
    param ($filter)
    if ($filter -match "\S") {
        ls -Recurse -Filter $filter -ErrorAction SilentlyContinue | select -Property FullName
    } else {
        Write-Error -Message "parameter filter is empty"
    }
}
function mem { (Get-CimInstance Win32_PhysicalMemory | Measure-Object -Property capacity -Sum).sum /1gb }
# print shell commands
function get-shell { ls "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions" | Get-ItemPropertyValue -Name Name | sort }
# open file, selection with fzf
function invoke-fzf { ((ls -File).FullName | fzf ) -replace ' ','` ' | iex }
function kal { C:\Users\ThinkPad\Apps\cygwin\bin\cal.exe "-3wm" }
function teams { start "$ENV:APPDATA\Microsoft\Windows\Start Menu\Programs\Microsoft Teams.lnk" }
function el { eza --icons --header -Fl }
function ef { eza --icons -F }
function ndr2 {
  Write-Host -ForegroundColor Yellow -NoNewline "[i]"
  Write-Host " NDR2 stram abspielen"
  mpv https://icecast.ndr.de/ndr/ndr2/niedersachsen/mp3/128/stream.mp3
}
function prompt {
    $myPC = [System.Net.Dns]::GetHostByName($null) | select -ExpandProperty HostName
    $IsAdmin = (New-Object Security.Principal.WindowsPrincipal ([Security.Principal.WindowsIdentity]::GetCurrent())).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
    if ($ENV:WT_SESSION -ne $null) {
      Write-Host -NoNewline "┌──["
      Write-Host -ForegroundColor DarkBlue -NoNewline "󰨊 "
      Write-Host -NoNewline "]─("
    } else {
      Write-Host -NoNewline "┌──("
    }
    Write-Host -ForegroundColor DarkGreen -NoNewline "$env:USERNAME"
    Write-Host -ForegroundColor DarkYellow -NoNewline "@"
    Write-Host -ForegroundColor DarkBlue -NoNewline "$myPC"
    Write-Host ")─[$PWD]"
    if ($IsAdmin) { "└─🔥 " }
    else { "└─ " }
}
function toggle-VMAdapter {
    $Name = 'VMware Network Adapter VMnet1'
    if ((Get-NetAdapter -Name $Name).Status -eq 'Up') {
        Write-Host -NoNewline 'NetAdapter '
        Write-Host -ForegroundColor DarkRed -NoNewline $Name
        Write-Host ' deaktivieren'
        disable-NetAdapter -Name $Name
    } else {
        Write-Host -NoNewline 'NetAdapter '
        Write-Host -ForegroundColor DarkRed -NoNewline $Name
        Write-Host ' aktivieren'
        enable-NetAdapter -Name $Name
    }
}
function myipconfig {
  # function brings colors in ipconfig
  $content = ipconfig
  foreach ($line in $content)
  {
    # if line end with colon
    if ($line -match ".+:$") {
      Write-Host $line -ForegroundColor DarkGreen
      # if line end with ip-address
    } elseif ($line -match "[0-9abcdef\.:%]{7,}$") {
      $split = $line -split ": "
      if ($split.Length -gt 1) {
        # print type
        Write-Host $split[0].TrimEnd() ": " -NoNewline -ForegroundColor DarkGray
        # print ip-address
        Write-Host $split[1] -ForegroundColor DarkRed
      } else {
        Write-Host $line -ForegroundColor DarkRed
      }
    } else {
      Write-Host $line
    }
  }
}
